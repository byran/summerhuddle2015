<?php
set_include_path('/home/pi/Source_Code/web/www');

require_once 'functions.php';


class ExtendedReportTest extends PHPUnit_Framework_TestCase
{
    public function testExampleFileThatPassesAllTestsReturnsFiveStars()
    {
$mock_pass = '{"metrics":[' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "pass"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "pass"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "pass"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "pass"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "pass"' .
' }' . 
']}';

        $this->assertEquals(5, countStars($mock_pass));
    }

    public function testExampleFileThatFailsAllTestsReturnsZeroStars()
    {
$mock_fail = '{"metrics":[' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' }' . 
']}';

        $this->assertEquals(0, countStars($mock_fail));
    }

    public function testMetricTableIsFormed()
    {
$mock_data = '{"metrics":[' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' },' . 
' {' .
' "name": "depth",' .
' "value": 1,' .
' "range": "10-20",' .
' "result": "fail"' .
' }' . 
']}';

        $this->assertNotEquals("", makeTable($mock_data));
    }

}
?>
