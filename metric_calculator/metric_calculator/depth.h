#ifndef DEPTH_INCLUDED
#define DEPTH_INCLUDED

#include "metric.h"
#include <vector>

struct depth_metric_type
{
    enum value {MAX , AVERAGE};
};

class depth : public metric
{
public:
    depth(depth_metric_type::value depth_type); //This is the constructor

    virtual void parse_line(const std::string & line);
    int metric_result();
private:
    int depth_count;
    int number_of_functions;
    std::vector <int> depths;
    depth_metric_type::value depth_type;

};


#endif
