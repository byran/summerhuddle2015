#include "detailed_metric_result.h"

using namespace std;

bool detailed_metric_result::operator==(const detailed_metric_result& rhs) const
{
    return name == rhs.name &&
        value == rhs.value &&
        threshold == rhs.threshold &&
        result == rhs.result;
}

string detailed_metric_result::to_json()
{
    stringstream os;

    os << "{\"name\":\"" << name << "\","
    << "\"value\":" << value << ","
    << "\"range\":\"" << threshold << "\","
    << "\"result\":\"" << (result ? "pass" : "fail") << "\"}";

    return os.str();
}
