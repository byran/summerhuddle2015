#include "function_lines.h"
#include <string>
#include <vector>

const int average_threshold=25;
const int max_threshold=30;

using namespace std;

function_lines::function_lines(function_metric_type::value function_type) : line_count(1, 0), function_type(function_type)
{
    number_of_lines=1;
    depth_count=0;
    number_of_functions=0;
}

void function_lines::parse_line(const std::string &line )
{
    for(size_t i=0; i<line.length();i++)
    {
        if (line[i]=='{')
            depth_count++;

        if (line[i]=='}')
        {
            depth_count--;

            if (depth_count==0)
            {
                line_count[number_of_functions]=number_of_lines;
                number_of_functions++;
                line_count.push_back (0);
                number_of_lines=1;
            }
        }
    }
    if (depth_count>0)
        number_of_lines++;
}

float function_lines::average_lines_per_function()
{
    float sum=0;
    for (int i=0; i<number_of_functions;i++)
    {
        sum+=line_count[i];
    }
    if (number_of_functions==0)
        return 0;

    return sum/number_of_functions;
}

int function_lines::metric_result()
{

 	int max=0;

    if (function_type==function_metric_type::AVERAGE)
    {
        if (average_lines_per_function()<average_threshold)
            return 1;
        return 0;
    }

    // must be MAX

    for(size_t i=0; i<line_count.size();i++)
	{
		if(max<line_count[i])
		   max=line_count[i];
	}

	if (max<max_threshold)
        return 1;
    return 0;

}

detailed_metric_result function_lines::get_result()
{
    detailed_metric_result result;

    result.name = "Function lines";
    result.value = static_cast<int>(average_lines_per_function());
    stringstream s;
    s << threshold;
    result.threshold = ">= " + s.str();

    result.result = metric_result();

    return result;
}
