#include "depth.h"
#include <string>
#include <vector>

const int max_threshold=6;
const size_t average_threshold=5;

using namespace std;

depth::depth(depth_metric_type::value depth_type) : depths(1, 0), depth_type(depth_type)
{
    depth_count=0;
    number_of_functions=0;
}

void depth::parse_line(const std::string &line )
{
    for(size_t i=0; i<line.length();i++)
    {
        if (line[i]=='{')
            depth_count++;
	    if(depths[number_of_functions]<depth_count)
		depths[number_of_functions]=depth_count;

        if (line[i]=='}')
        {
            depth_count--;

            if (depth_count==0)
            {
                number_of_functions++;
                depths.push_back (0);
            }
        }
    }
}

int depth::metric_result()
{

	int max=0;
	size_t average=0;
	for(size_t i=0; i<depths.size();i++)
	{
	    average+=depths[i];
		if(max<depths[i])
		max=depths[i];
	}
    if (depth_type==depth_metric_type::MAX)
    {
        if (max<max_threshold)
            return 1;
        return 0;
    }

        average/=(depths.size()-1);
        if (average<average_threshold)
            return 1;

        return 0;

}
