#ifndef COUNTER_H_INCLUDED
#define COUNTER_H_INCLUDED

#include <string>

int counter(const std::string& line, const std::string& token);

#endif // COUNTER_H_INCLUDED
