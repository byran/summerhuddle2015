#include "cyclomatic_complexity.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace std;

TEST(if_counter, empty_file_returns_zero_counters_and_low_scores)
{
    cyclomatic_complexity c;

    ASSERT_EQ(0, c.if_count());
    ASSERT_EQ(0, c.for_count());
    ASSERT_FALSE(c.if_score());
    ASSERT_FALSE(c.if_and_for_density_score());
}

TEST(if_counter, no_ifs_in_first_line_gives_0)
{
    cyclomatic_complexity c;
    c.parse_line("Hello World!");

    ASSERT_EQ(0, c.if_count());
}

TEST(if_counter, one_if_in_first_line_gives_1)
{
    cyclomatic_complexity c;
    c.parse_line("if");

    ASSERT_EQ(1, c.if_count());
}

TEST(if_counter, two_if_in_same_line_gives_2)
{
    cyclomatic_complexity c;
    c.parse_line("if and another if");

    ASSERT_EQ(2, c.if_count());
}

TEST(if_counter,  two_if_in_different_lines_gives_2)
{
    cyclomatic_complexity c;
    c.parse_line("if");
    c.parse_line(" and ");
    c.parse_line("another");
    c.parse_line(" if");

    ASSERT_EQ(2, c.if_count());
}

TEST(if_score, two_ifs_scores_false_which_means_low)
{
    cyclomatic_complexity c;
    c.parse_line("if");
    c.parse_line(" and ");
    c.parse_line("another");
    c.parse_line(" if");

    ASSERT_FALSE(c.if_score());
}

TEST(if_score, seventy_seven_ifs_returns_true_which_means_high)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 77; ++i)
        c.parse_line("if");

    ASSERT_EQ(true, c.if_score());
}

TEST(if_and_for_density_score, twenty_ifs_and_fors_in_80_lines_returns_true_which_means_high)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 20; ++i)
        c.parse_line("if");
    for (int i = 0; i < 20; ++i)
        c.parse_line("for");
    for (int i = 0; i < 40; ++i)
        c.parse_line("Hallo Mundo!");

    ASSERT_EQ(true, c.if_and_for_density_score());
}

TEST(if_and_for_density_score, twenty_ifs_and_fors_in_81_lines_returns_false_which_means_low)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 20; ++i)
        c.parse_line("if");
    for (int i = 0; i < 20; ++i)
        c.parse_line("for");
    for (int i = 0; i < 41; ++i)
        c.parse_line("Hallo Mundo!");

    ASSERT_FALSE(c.if_and_for_density_score());
}

TEST(for_counter, no_fors_in_first_line_returns_0)
{
    cyclomatic_complexity c;
    c.parse_line("Hello World!");

    ASSERT_EQ(0, c.for_count());
}

TEST(for_counter, one_for_in_one_line_returns_1)
{
    cyclomatic_complexity c;

    c.parse_line("for");

    ASSERT_EQ(1, c.for_count());
}

TEST(for_counter, two_for_in_same_line_gives_2)
{
    cyclomatic_complexity c;
    c.parse_line("for and another for");

    ASSERT_EQ(2, c.for_count());
}

TEST(for_counter, two_for_in_different_lines_gives_2)
{
    cyclomatic_complexity c;
    c.parse_line("for");
    c.parse_line(" and ");
    c.parse_line("another");
    c.parse_line(" for");

    ASSERT_EQ(2, c.for_count());
}

TEST(cyclomatic_complexity, all_low_translates_to_1_which_is_good)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 9; ++i)
        c.parse_line("Hallo Mundo!");

    ASSERT_EQ(1, c.metric_result());
}

TEST(cyclomatic_complexity, if_score_high_translates_to_0_which_is_bad)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 25; ++i)
        c.parse_line("if");

    ASSERT_EQ(0, c.metric_result());
}

TEST(cyclomatic_complexity, fifteen_ifs_and_fors_translates_to_0_which_is_bad)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 15; ++i)
        c.parse_line("if");
    for (int i = 0; i < 15; ++i)
        c.parse_line("for");

    ASSERT_EQ(0, c.metric_result());
}

TEST(cyclomatic_complexity, for_density_high_translates_to_0_which_is_bad)
{
    cyclomatic_complexity c;
    c.parse_line("for");

    ASSERT_EQ(0, c.metric_result());
}

TEST(cyclomatic_complexity, fifteen_ifs_and_fors_detailed_result)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 15; ++i)
        c.parse_line("if");
    for (int i = 0; i < 15; ++i)
        c.parse_line("for");

    detailed_metric_result expected_result;
    expected_result.name = "Cyclomatic complexity";
    expected_result.value = 100 * 30 / 30;
    expected_result.threshold = ">= 50";
    expected_result.result = false;

    ASSERT_EQ(expected_result, c.get_result());
}


TEST(cyclomatic_complexity, all_low_detailed_report)
{
    cyclomatic_complexity c;
    for (int i = 0; i < 9; ++i)
        c.parse_line("Hallo Mundo!");

    detailed_metric_result expected_result;
    expected_result.name = "Cyclomatic complexity";
    expected_result.value = 100 * 0 / 9;
    expected_result.threshold = ">= 50";
    expected_result.result = true;

    ASSERT_EQ(expected_result, c.get_result());
}
