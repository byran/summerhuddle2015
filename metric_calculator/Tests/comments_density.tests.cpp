#include "comments_density.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace std;


TEST(comments_density, uncommented_lines_gives_0)
{
    comments_density hello_comments;
    hello_comments.parse_line("Hello World");
    hello_comments.parse_line("fox ");
    hello_comments.parse_line("Hello/ World");

    ASSERT_EQ(0, hello_comments.metric_result());
}

TEST(comments_density, one_line_consisting_of_half_the_characters_in_a_comment_gives_1)
{
    comments_density hello_comments;
    hello_comments.parse_line("not a comment//Hello World");

    ASSERT_EQ(0, hello_comments.metric_result());
}

TEST(comments_density, multiple_lines_with_less_than_10_percent_commented_gives_1)
{
    comments_density hello_comments;
    hello_comments.parse_line("Hello World");
    hello_comments.parse_line("fox dog cat //d");
    hello_comments.parse_line("Hello/ World");

    ASSERT_EQ(1, hello_comments.metric_result());
}

TEST(comments_density, fully_commented_out_line_gives_0)
{
    comments_density hello_comments;
    hello_comments.parse_line("//Hello //World");

    ASSERT_EQ(0, hello_comments.metric_result());
}


std::ostream& operator<<(std::ostream &s, const detailed_metric_result& actual_result)
{
    return s << "Name: " << actual_result.name << endl
    << "Value: " << actual_result.value << endl
    << "Threshold: " << actual_result.threshold << endl
    << "Result: " << actual_result.result << endl;
}

TEST(comments_density, fully_commented_out_detailed_result)
{
    comments_density hello_comments;
    hello_comments.parse_line("//Hello //World");

    detailed_metric_result expected_result;
    expected_result.name = "Comment density";
    expected_result.value = 15 * 100 / 15;
    expected_result.threshold = "5% to 50%";
    expected_result.result = false;
    ASSERT_EQ(expected_result, hello_comments.get_result());
}

TEST(comments_density, multiple_lines_with_less_than_10_percent_commented_detailed_result)
{
    comments_density hello_comments;
    hello_comments.parse_line("Hello World");
    hello_comments.parse_line("fox dog cat //d");
    hello_comments.parse_line("Hello/ World");

    detailed_metric_result expected_result;
    expected_result.name = "Comment density";
    expected_result.value = 3 * 100 / 38;
    expected_result.threshold = "5% to 50%";
    expected_result.result = true;

    ASSERT_EQ(expected_result, hello_comments.get_result());
}
