#include "parameters.h"
#include "cyclomatic_complexity.h"
#include "function_lines.h"
#include "comments_density.h"
#include "depth.h"
#include "line_sender.h"
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    parameters fileOpener(argc, argv);

    cyclomatic_complexity cyclomatic_metric;
    function_lines function_average_metric(function_metric_type::AVERAGE);
    comments_density comments_metric;
    depth depth_max_metric(depth_metric_type::MAX);
    depth depth_average_metric(depth_metric_type::AVERAGE);
    function_lines function_max_metric(function_metric_type::MAX);

    line_sender sender;

    sender.add_metric(&cyclomatic_metric);
    sender.add_metric(&function_average_metric);
    sender.add_metric(&comments_metric);
    sender.add_metric(&depth_max_metric);
    sender.add_metric(&depth_average_metric);
    sender.add_metric(&function_max_metric);

    sender.extract_lines(fileOpener.input_stream());

    cout << "{\"metric\" : [";
    cout << cyclomatic_metric.get_result().to_json() << ",";
    cout << function_metric.get_result().to_json() << ",";
    cout << comments_metric.get_result().to_json();
    cout << "]}";

    return sender.calculate_star_rating();

}
